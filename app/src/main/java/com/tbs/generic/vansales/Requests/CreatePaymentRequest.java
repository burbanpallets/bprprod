package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.CreatePaymentDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CreatePaymentRequest extends AsyncTask<String, Void, Boolean> {

    private CreatePaymentDO createPaymentDO;
    private Context mContext;
    private String id, check, date, bank,encImage;
    private int type;
    private Double amount;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String message = "";

    public CreatePaymentRequest(String Id, String Check, String Date, String Bank, Double amounT, int Type,String encImagE, Context mContext) {

        this.mContext = mContext;
        this.id = Id;
        this.check = Check;
        this.date = Date;
        this.bank = Bank;
        this.type = Type;
        this.amount = amounT;
        this.encImage = encImagE;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, CreatePaymentDO createPaymentDO,String msg);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();
        ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(mContext).getActiveDeliveryMainDo(mContext);
        CustomerDo customerDo = StorageManager.getInstance(mContext).getCurrentSpotSalesCustomer(mContext);
        String ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, mContext.getResources().getString(R.string.checkin_non_scheduled));
        String role = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");

        try {
            if (ShipmentType.equals(mContext.getResources().getString(R.string.checkin_non_scheduled))) {

                jsonObject.put("I_YBPR", customerDo.customer);
            } else {
                jsonObject.put("I_YBPR", activeDeliverySavedDo.customer);
            }
            jsonObject.put("I_YCREUSR", role);
            jsonObject.put("I_YPAYTYP", type);
            jsonObject.put("I_YOAMT", amount);
            jsonObject.put("I_YCHKDATE", date);
            jsonObject.put("I_YCHKNO", check);
            jsonObject.put("I_YBANK", bank);
            jsonObject.put("I_YCHQIMG", encImage);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }

        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.PAYMENT_PAYALL, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("multiple xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            createPaymentDO = new CreatePaymentDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YPAYNO")) {
                            if (text.length() > 0) {

                                createPaymentDO.paymentNumber = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YSINVNO")) {
                            if (text.length() > 0) {

                                createPaymentDO.invoiceNumber = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YFCY")) {
                            createPaymentDO.site = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YGMESSAGE")) {
                            createPaymentDO.message = text;

                        }else if (attribute.equalsIgnoreCase("O_YBP")) {
                            createPaymentDO.customer = text;

                        } else if (attribute.equalsIgnoreCase("O_YBPRSAC")) {
                            createPaymentDO.control = text;

                        } else if (attribute.equalsIgnoreCase("O_YACC")) {
                            createPaymentDO.account = text;

                        } else if (attribute.equalsIgnoreCase("O_YACCDAT")) {
                            createPaymentDO.accountingDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YBAN")) {
                            createPaymentDO.bank = text;

                        } else if (attribute.equalsIgnoreCase("I_YAMT")) {
                            createPaymentDO.amount = text;

                        } else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            createPaymentDO.currency = text;

                        } else if (attribute.equalsIgnoreCase("O_YBPAMT")) {
                            createPaymentDO.amount = text;

                        } else if (attribute.equalsIgnoreCase("O_YCHQNUM")) {
                            createPaymentDO.checkNumber = text;

                        } else if (attribute.equalsIgnoreCase("O_YBPAINV")) {
                            createPaymentDO.address = text;

                        } else if (attribute.equalsIgnoreCase("O_YBANAMT")) {
                            createPaymentDO.bankAmount = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YDOC")) {
                            if(text.length()>0){
                                createPaymentDO.doc= Integer.parseInt(text);

                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YPRI")) {
                            if(text.length()>0){
                                createPaymentDO.email= Integer.parseInt(text);

                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YEMAIL")) {
                            if(text.length()>0){
                                createPaymentDO.print= Integer.parseInt(text);

                            }

                        }
                        text = "";

                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, createPaymentDO,ServiceURLS.message);
        }
    }
}