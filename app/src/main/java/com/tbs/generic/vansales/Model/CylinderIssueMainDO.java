package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class CylinderIssueMainDO implements Serializable {


    public String shipmentNumber  = "";
    public String site            = "";
    public String siteDescription = "";
    public String siteAddress1    = "";
    public String siteAddress2    = "";
    public String siteAddress3    = "";
    public String countryName     = "";
    public String webSite         = "";
    public String landLine = "";
    public String email = "";
    public String email2 = "";
    public String logo = "";
    public String customerStreet      = "";
    public String customerLandMark    = "";
    public String customerTown        = "";
    public String customerCity        = "";
    public String company  = "";
    public String companyCode  = "";

    public String customerPostalCode        = "";

    public String siteCountry = "";
    public String siteCity = "";
    public String sitePostalCode = "";
    public String siteLandLine = "";
    public String siteMobile = "";
    public String siteFax = "";
    public String siteEmail1 = "";
    public String siteEmail2 = "";
    public String siteWebEmail = "";
    public String fax = "";
    public String mobile = "";
    public String customer = "";
    public String customerDescription = "";

    public String customercountryName = "";
    public String customercity = "";
    public String customerpostalCode = "";
    public String customerlandLine = "";
    public String customeremail = "";
    public String customeremail2 = "";
    public String signature;
    public String createUserName;
    public String createUserID;
    public String createdTime;
    public String createdDate;

    public ArrayList<CylinderIssueDO> cylinderIssueDOS = new ArrayList<>();


}
