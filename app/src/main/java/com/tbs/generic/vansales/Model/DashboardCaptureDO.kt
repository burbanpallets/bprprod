package com.tbs.generic.vansales.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 *Created by kishoreganji on 21-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
data class DashboardCaptureDO(
        @SerializedName("time") var time: String? = "",
        @SerializedName("date") var date: String? = "",
        @SerializedName("lattitude") var lattitude: String? = "",
        @SerializedName("longitude") var longitude: String? = "") : Serializable