package com.tbs.generic.vansales.Activitys

import android.annotation.TargetApi
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.iid.FirebaseInstanceId
import com.tbs.generic.pod.Activitys.MasterDashboardActivity
import com.tbs.generic.pod.Activitys.StartDayActivity
import com.tbs.generic.vansales.Model.DashboardCaptureDO
import com.tbs.generic.vansales.Model.PickUpDo
import com.tbs.generic.vansales.Model.VehicleCheckInDo
import com.tbs.generic.vansales.Model.VehicleCheckOutDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.collector.CollectorCustomerActivity
import com.tbs.generic.vansales.common.DashboardOperations
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.dialogs.MenuOptionsDialog
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.*
import kotlinx.android.synthetic.main.activity_dashboard.iv_menu
import kotlinx.android.synthetic.main.activity_dashboard.iv_profile_pic
import kotlinx.android.synthetic.main.activity_dashboard.ll_data
import kotlinx.android.synthetic.main.activity_dashboard.ll_status
import kotlinx.android.synthetic.main.activity_dashboard.tv_company_code
import kotlinx.android.synthetic.main.activity_dashboard.tv_date
import kotlinx.android.synthetic.main.activity_dashboard.tv_driver_code
import kotlinx.android.synthetic.main.activity_dashboard.tv_driver_name
import kotlinx.android.synthetic.main.activity_dashboard.tv_plate
import kotlinx.android.synthetic.main.activity_dashboard.tv_routing_id
import kotlinx.android.synthetic.main.activity_dashboard.tv_shipments
import kotlinx.android.synthetic.main.activity_dashboard.tv_status
import kotlinx.android.synthetic.main.activity_dashboard.tv_vehicle_code
import kotlinx.android.synthetic.main.activity_dashboard.vehicleLabel
import kotlinx.android.synthetic.main.activity_dashboard_new.*
import kotlinx.android.synthetic.main.activity_schedule_non_schedule.*
import kotlinx.android.synthetic.main.closing_reading_dialog.*
import kotlinx.android.synthetic.main.include_dashboard_flow.*
import kotlinx.android.synthetic.main.include_dashboard_steps.imv_tick_check_in
import kotlinx.android.synthetic.main.include_dashboard_steps.llCheckOut
import kotlinx.android.synthetic.main.include_dashboard_steps.llPaymentReciept
import kotlinx.android.synthetic.main.include_dashboard_steps.llScheduledSales
import kotlinx.android.synthetic.main.include_dashboard_steps.llSpotSales
import kotlinx.android.synthetic.main.include_dashboard_steps.llStartDay
import kotlinx.android.synthetic.main.include_dashboard_steps.ll_check_in
import kotlinx.android.synthetic.main.include_dashboard_steps.ll_open_stock
import kotlinx.android.synthetic.main.include_dashboard_summary.*
import kotlinx.android.synthetic.main.include_eta_etd.*
import kotlinx.android.synthetic.main.include_status_job.*
import kotlinx.android.synthetic.main.include_trip_summary.*
import kotlinx.android.synthetic.main.reading_dialog.button1
import kotlinx.android.synthetic.main.reading_dialog.button2
import java.math.BigDecimal


class DashBoardActivity : BaseActivity() {
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200
    lateinit var vehicleCheckInDo: VehicleCheckInDo
    private var pickUpDos: ArrayList<PickUpDo> = ArrayList()
    var isDepartureExisted = false
    lateinit var reading: String
    var operation: Int = 4
    lateinit var closereading: String


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.activity_dashboard_new, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        flToolbar.visibility = View.GONE
        changeLocale()
        initializeControls()
        insertDummyContactWrapper()

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener({

            if (!it.isSuccessful) {
                Log.w("Token", "getInstanceId failed", it.exception)
                return@addOnCompleteListener
            }

            // Get new Instance ID token
            val token = it.result?.token

            // Log and toast
            Log.d("Token_Change", "token--" + token)
        })
    }


    @TargetApi(Build.VERSION_CODES.M)
    private fun insertDummyContactWrapper() {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera")
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Location")
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (permissionsList.size > 0) {
            requestPermissions(permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            return
        }
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    override fun initializeControls() {
        //ivBack.visibility = View.GONE
        ivMenu.visibility = View.VISIBLE
        ivPic!!.visibility = View.VISIBLE
        preferenceUtils = PreferenceUtils(this)
        tvScreenTitle.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
//        var face = Typeface.createFromAsset(getAssets(), "bgl.ttf");
//        tvScreenTitle.setTypeface(face)
        vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckOutData(this)
        //  disableMenuWithBackButton();

        if (vehicleCheckInDo != null) {
            vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckOutData(this)

        } else {
            vehicleCheckInDo = VehicleCheckInDo()

        }

        initUserData()
        if (preferenceUtils.getIntFromPreference(PreferenceUtils.OPERATION, 0).equals(3)) {
            operation = 3
            swStatus.isChecked = true
        } else {
            operation = 4
            swStatus.isChecked = false

        }


        llStartDay.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this@DashBoardActivity, StartDayActivity::class.java)
            startActivity(intent)
        }

        ll_check_in.setOnClickListener {
            Util.preventTwoClick(it)
            DashboardOperations().checkInProcess(this)
            /*  val intent = Intent(this@DashBoardActivity, CheckinScreen::class.java)
              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
              startActivity(intent)*/
//            if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
//                showAppCompatAlert(getString(R.string.alert), getString(R.string.check_in_process_done), getString(R.string.ok), "", getString(R.string.failure), false)
//
//            } else {
//                DashboardOperations().checkInProcess(this)
//
//            }

        }
        swStatus.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                preferenceUtils.saveInt(PreferenceUtils.OPERATION, 3)
                operation = 3
                showLoader()
                location(ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        var captureDO = StorageManager.getInstance(this).getDashboardData(this)
                        captureDO.date = CalendarUtils.getDate()
                        captureDO.time = CalendarUtils.getTime()
                        captureDO.lattitude = lattitudeFused
                        captureDO.longitude = longitudeFused
                        StorageManager.getInstance(this).insertDashboardData(this, captureDO)

                        vrSuccess(3)

                    }
                })
            } else {
                preferenceUtils.saveInt(PreferenceUtils.OPERATION, 4)

                operation = 4
                showLoader()
                location(ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        var captureDO = StorageManager.getInstance(this).getDashboardData(this)
                        captureDO.date = CalendarUtils.getDate()
                        captureDO.time = CalendarUtils.getTime()
                        captureDO.lattitude = lattitudeFused
                        captureDO.longitude = longitudeFused
                        StorageManager.getInstance(this).insertDashboardData(this, captureDO)
                        vrSuccess(4)

                    }
                })
            }
        }

        llScheduledSales.setOnClickListener {
            Util.preventTwoClick(it)
            /*val intent = Intent(this@DashBoardActivity, ScheduledSalesActivity::class.java)
            startActivity(intent)*/
            if (operation == 3) {
                scheduleSales()
            } else {

            }


        }
        llSpotSales.setOnClickListener {
            Util.preventTwoClick(it)
            if (operation == 3) {
                spotPickups()
            }
//            val intent = Intent(this@DashBoardActivity, SpotPICKUPSActivity::class.java)
//            startActivity(intent)
        }
//        llMasterData.setOnClickListener {
//            val intent = Intent(this@DashBoardActivity, MasterDataActivity::class.java)
//            startActivity(intent)
//        }
        llPaymentReciept.setOnClickListener {
            Util.preventTwoClick(it)
            //            if (isExpired()) {
//                showAppCompatAlert("", "You are not authorized to use this function", "Ok", "", "Expired", false);
//            } else {
//                val intent = Intent(this@DashBoardActivity, InvoiceListActivity::class.java)
//                startActivity(intent)
//            }

            val intent = Intent(this@DashBoardActivity, CollectorCustomerActivity::class.java)
            startActivity(intent)
        }

        llCheckOut.setOnClickListener {
            Util.preventTwoClick(it)
            val vehicleCheckInDo = StorageManager.getInstance(this@DashBoardActivity).getVehicleCheckInData(this@DashBoardActivity)
            if (!vehicleCheckInDo.checkInStatus!!.isEmpty() || AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
                var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                if (shipmentId == null || shipmentId.equals("")) {
                    val customerDo = StorageManager.getInstance(this@DashBoardActivity).getCurrentSpotSalesCustomer(this@DashBoardActivity)
                    if (customerDo.customer.equals("")) {
                        loadData()
//                        val intent = Intent(this@DashBoardActivity, CheckOutScreen::class.java)
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                        startActivity(intent)
                    } else {
                        showAppCompatAlert("", "Please process the current customer " + customerDo.customer + "'s non schedule shipment", getString(R.string.ok), "", getString(R.string.failure), false)
                    }

                } else {
                    showToast(getString(R.string.please_process_curr_sche) + shipmentId + getString(R.string.shipment))
                }


            } else {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.please_finish_check_in_process), getString(R.string.ok), "", getString(R.string.failure), false)
            }
        }



        ll_open_stock.setOnClickListener {
            Util.preventTwoClick(it)
            val vehicleCheckInDo = StorageManager.getInstance(this@DashBoardActivity).getVehicleCheckInData(this@DashBoardActivity)
            if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                if (!vehicleCheckInDo.loadStock.equals("")) {
                    /* val intent = Intent(this@DashBoardActivity, LoadStockStatusActivity::class.java)
                     intent.putExtra("FLAG", 1);
                     intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                     intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                     startActivity(intent)*/
                    val intent = Intent(this, ScheduleNonScheduleActivity::class.java)
                    intent.putExtra(Constants.SCREEN_TYPE, getString(R.string.opening_stock))
                    startActivity(intent)
                } else {
                    showToast(getString(R.string.please_complete_stock_load))
                }
            } else {

                if (!vehicleCheckInDo.loadStock.equals("")) {
                    /*val intent = Intent(this@DashBoardActivity, LoadStockStatusActivity::class.java)
                    intent.putExtra("FLAG", 1);
                    intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)*/
                    val intent = Intent(this, ScheduleNonScheduleActivity::class.java)
                    intent.putExtra(Constants.SCREEN_TYPE, getString(R.string.opening_stock))
                    startActivity(intent)
                } else {
                    showAppCompatAlert("", getString(R.string.please_finish_check_in_process), getString(R.string.ok), "", "", false)
                }

            }
        }

        iv_menu.setOnClickListener {
            Util.preventTwoClick(it)

            MenuOptionsDialog().show(supportFragmentManager, "MenuOptionsDialog")
        }

        ll_user_activity_report.setOnClickListener {
            //            val vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
//            val status = vehicleCheckInDo.checkInStatus
//            if (!status!!.isEmpty()) {
//                val intent = Intent(this, UserActivityReportActivity::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivity(intent)
//            } else {
//                //showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)
//
//                YesOrNoDialogFragment().newInstance("Alert", "Please Finish Check-in Process", ResultListner { `object`, isSuccess ->
//
//
//                }, false).show(this?.supportFragmentManager!!, "YesOrNoDialogFragment")
//            }
        }
        llmasterdata.setOnClickListener {
            val intent = Intent(this, MasterDashboardActivity::class.java)
            startActivity(intent)
        }

        llVehicelStock.setOnClickListener {
            val intent = Intent(this, LoadStockStatusActivity::class.java)
            intent.putExtra(Constants.SCREEN_TYPE, getString(R.string.vehicle_stock))
            startActivity(intent)
        }
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.INSTRUCTIONS, "").isNotEmpty()) {
            tvInstruction.visibility = View.VISIBLE
        } else {
            tvInstruction.visibility = View.GONE
        }
        tvInstruction.setOnClickListener {
            showInstructions()
        }

        btn_main.setOnClickListener {

            ll_steps_dashboard.visibility = View.VISIBLE
            ll_summary.visibility = View.GONE
            ll_driver_details.visibility = View.VISIBLE
            if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
                ll_eta_etd.visibility = View.VISIBLE
            } else {
                ll_eta_etd.visibility = View.GONE
            }

            btn_main.setBackgroundResource(R.drawable.rounded_white_shape_left_bottom)
            btn_summry.setBackgroundResource(R.drawable.rounded_shape_right_bottom)
            btn_main.setTextColor(ContextCompat.getColor(this, R.color.app_color_green))
            btn_summry.setTextColor(ContextCompat.getColor(this, R.color.white))

        }

        btn_summry.setOnClickListener {
            ll_steps_dashboard.visibility = View.GONE
            ll_summary.visibility = View.VISIBLE
            ll_driver_details.visibility = View.GONE
            ll_eta_etd.visibility = View.GONE
            btn_main.setBackgroundResource(R.drawable.rounded_shape_left_bottom)
            btn_summry.setBackgroundResource(R.drawable.rounded_white_shape_right_bottom)
            btn_main.setTextColor(ContextCompat.getColor(this, R.color.white))
            btn_summry.setTextColor(ContextCompat.getColor(this, R.color.app_color_green))
            showLoader()
            val request = SummaryRequest(this)
            request.setOnResultListener { isError, modelDO ->
                if (isError) {
                    hideLoader()

                    Util.showToast(this, getString(R.string.server_error))
                } else {

                    if (modelDO != null) {
                        if (modelDO.flag == 20) {

                            hideLoader()

                            var percentage = modelDO.percentage.toString()
                            var totalTime = modelDO.totalTime.toString()
                            var totalTravelledDistance = modelDO.totalTravelledDistance.toString()
                            var completedStops = modelDO.completedStops.toString()
                            var totalTravelledTime = modelDO.totalTravelledTime.toString()
                            var totalStops = modelDO.totalStops.toString()
                            var totalDistance = modelDO.totalDistance.toString()

                            tvPercentage.setText(percentage + " %")
                            tvTotalDrops.setText("" + modelDO.totalDrops)
                            tvTotalPickups.setText("" + modelDO.totalPickups)
                            tvCompletedStops.setText("" + modelDO.completedStops)
                            tvCompletedDrops.setText("" + modelDO.completedDrops)
                            tvCompletedPickups.setText("" + modelDO.completedPickups)
//                            tvTotDistance.setText(""+totalTravelledDistance+"/"+totalDistance)
                            tvTotaDistance.setText(" / " + totalDistance)
                            tvActualDistance.setText(totalTravelledDistance)
                            tvTotalTime.setText(" / " + totalTime)
                            tvActualTime.setText("" + totalTravelledTime)
                            tvUnits.setText(modelDO.distanceUNIT)
                            tv_ontime.setText("" + modelDO.onTIME.toString())

                            tvActualStops.setText(completedStops)

                            tvStops.setText(" / " + totalStops)
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                progressBar.setProgress(modelDO.percentage.toInt(), true)
                            }
                        } else {
                            hideLoader()


                            Util.showToast(this, modelDO.message)

                        }
                    } else {
                        hideLoader()

                        Util.showToast(this, getString(R.string.server_error))


                    }
                }

            }

            request.execute()

        }

    }

    private fun spotPickups() {
        val vehicleCheckOutDo = StorageManager.getInstance(this).getVehicleCheckOutData(this)
        if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
            val vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
            if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {

                val scheduleDOC = preferenceUtils.getStringFromPreference(PreferenceUtils.SCHEDULE_DOCUMENT, "")
                if (scheduleDOC.isEmpty()) {
                    val intent = Intent(this, SpotPICKUPSActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                } else {
                    showAppCompatAlert("", getString(R.string.please_process_curr_sche), getString(R.string.ok), "", getString(R.string.failure), false)
                }

            } else {
                showAppCompatAlert("", getString(R.string.please_finish_check_in_process), getString(R.string.ok), "", getString(R.string.failure), false)
            }
        } else {
            showAppCompatAlert("", getString(R.string.you_have_arrived), getString(R.string.ok), "", "", false)
        }
    }

    private fun scheduleSales() {
        val vehicleCheckOutDo = StorageManager.getInstance(this).getVehicleCheckOutData(this)
        if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
            val vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
            if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                val scheduleDOC = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_RECEIPT, "")
                if (scheduleDOC.isEmpty()) {
                    val vehicleRoutId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
                    if (vehicleRoutId.length > 0) {
                        val intent = Intent(this, TransactionList::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                    } else {
                        showAppCompatAlert(getString(R.string.alert), getString(R.string.no_shiments_found), getString(R.string.ok), "", getString(R.string.failure), false)
                    }
                } else {
                    showAppCompatAlert("", getString(R.string.please_process_the_current_spot), getString(R.string.ok), "", getString(R.string.failure), false)
                }


            } else {
                showAppCompatAlert("", getString(R.string.please_finish_check_in_process), getString(R.string.ok), "", getString(R.string.failure), false)
            }
        } else {
            showAppCompatAlert("", getString(R.string.you_have_arrived), getString(R.string.ok), "", "", false)
        }
    }

    private fun initUserData() {

        tv_company_code.text = "" + resources.getString(R.string.company_name)
        tv_driver_code.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
        var vehCode = preferenceUtils.getStringFromPreference(PreferenceUtils.CVEHICLE_CODE, "")
        if (!TextUtils.isEmpty(vehCode)) {
            tv_vehicle_code.visibility = View.VISIBLE
            vehicleLabel.visibility = View.VISIBLE
            tvColon.visibility = View.VISIBLE

            tv_vehicle_code.text = "" + vehCode
        } else {
            tv_vehicle_code.visibility = View.GONE
            vehicleLabel.visibility = View.GONE
            tvColon.visibility = View.GONE

        }
        val siteId = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_NAME, "")
        if (!TextUtils.isEmpty(siteId)) {
            tv_driver_name.visibility = View.VISIBLE
            tv_driver_name.text = "" + siteId
        } else {
            tv_driver_name.visibility = View.GONE
        }

        val image = preferenceUtils.getStringFromPreference(PreferenceUtils.PROFILE_PICTURE, "")
        if (image.isNotEmpty()) {
            val decodedString = android.util.Base64.decode(image, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            iv_profile_pic?.setImageBitmap(decodedByte)
        }

    }

    private fun checkInInfo() {
        val data = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")

        if (!TextUtils.isEmpty(data)) {
            ll_status.visibility = View.VISIBLE
            val vehicleCheckInDo = StorageManager.getInstance(this@DashBoardActivity).getVehicleCheckInData(this@DashBoardActivity)
            if (vehicleCheckInDo.statusflag.equals("3")) {
                tv_routing_id.visibility = View.VISIBLE

                tv_status.text = "Vehicle Checked-in"

            } else if (vehicleCheckInDo.statusflag.equals("4")) {
                tv_status.text = "Vehicle Checked-Out"
                imv_tick_check_in.visibility = View.GONE
                tv_routing_id.visibility = View.VISIBLE

            } else {
                if (vehicleCheckInDo.checkInStatus.equals("", true)) {
                    if (!vehicleCheckInDo.loadStock.equals("")) {
                        tv_status.text = vehicleCheckInDo.loadStock
                    } else {
                        tv_status.text = getString(R.string.logged_in)
                        tv_routing_id.visibility = View.GONE

                    }
                } else {
                    tv_status.text = vehicleCheckInDo.checkInStatus
                }
            }



            ll_eta_etd.visibility = View.VISIBLE
            ll_data.visibility = View.GONE

            var caDate = AppPrefs.getString(AppPrefs.CA_DATE, "");
            var caTime = AppPrefs.getString(AppPrefs.CA_TIME, "");
            if (!TextUtils.isEmpty(caDate) && !TextUtils.isEmpty(caTime)) {
                val dMonth: String = caDate.substring(4, 6)
                val dyear: String = caDate.substring(0, 4)
                val dDate: String = caDate.substring(Math.max(caDate.length - 2, 0))
                val time1: String = caTime.substring(0, 2)
                val time2: String = caTime.substring(2, 4)

                tv_eta.text = dMonth + "-" + dDate + "-" + dyear + "  " + time1 + ":" + time2
                tv_ata.text = vehicleCheckInDo.actualvehicleCheckin!!.replace("/", "-")
                tv_atd.text = vehicleCheckInDo.actualvehicleCheckout!!.replace("/", "-")

            }

            var cdDate = AppPrefs.getString(AppPrefs.CD_DATE, "");
            var cdTime = AppPrefs.getString(AppPrefs.CD_TIME, "");

            if (!TextUtils.isEmpty(cdDate) && !TextUtils.isEmpty(cdTime)) {
                tv_etd.visibility = View.VISIBLE

                val dMonth: String = cdDate.substring(4, 6)
                val dyear: String = cdDate.substring(0, 4)
                val dDate: String = cdDate.substring(Math.max(cdDate.length - 2, 0))
                val time1: String = cdTime.substring(0, 2)
                val time2: String = cdTime.substring(2, 4)

                tv_etd.text = dMonth + "-" + dDate + "-" + dyear + "  " + time1 + ":" + time2
            } else {
                tv_etd.visibility = View.GONE
            }

            var odoReading = preferenceUtils.getStringFromPreference(PreferenceUtils.OPENING_READING, "")
            val unit = preferenceUtils.getStringFromPreference(PreferenceUtils.ODO_UNIT, "")

            if (odoReading.isNullOrEmpty()) {
                tv_odo_start.setText(getString(R.string.startreadlabel) + " :  " + "0 " + unit);


            } else {
                try {
                    tv_odo_start.setText(getString(R.string.startreadlabel) + " :  " + BigDecimal(odoReading).stripTrailingZeros().toPlainString() + " " + unit);

                } catch (e: NumberFormatException) {

                }

            }

            var odoReadingEnd = preferenceUtils.getDoubleFromPreference(PreferenceUtils.CLOSING_READING, 0.0)
            tv_odo_end.setText(getString(R.string.end_reading) + "" + BigDecimal(odoReadingEnd).stripTrailingZeros().toPlainString() + " " + unit);

            tv_date.text = getString(R.string.date) + " :  " + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE, "")
            tv_routing_id.text = getString(R.string.vr_id) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            tv_plate.text = getString(R.string.name_plate) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "")
            tv_shipments.text = getString(R.string.no_of_shipments) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CN_SHIPMENTS, "")
            tvCheckedIn.setText("" + resources.getString(R.string.checked_in))


        } else {
            ll_status.visibility = View.VISIBLE
            ll_eta_etd.visibility = View.GONE
            ll_data.visibility = View.GONE

        }

        if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
            imv_tick_check_in.visibility = View.VISIBLE
            ll_vr_id.visibility = View.VISIBLE
            val routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
            tv_vr_id.text = routeId
            hideLoader()

        } else {
            imv_tick_check_in.visibility = View.GONE
            ll_vr_id.visibility = View.GONE
            hideLoader()

            checkinLoad()
        }

    }


    override fun onResume() {
        super.onResume()
        showLoader()

        btn_main.performClick()
        var vehicleCheckOutDo = StorageManager.getInstance(this).getCheckOutData(this)
        if (vehicleCheckOutDo.checkoutFlag.equals("2")) {
            ll_vr_id.visibility = View.VISIBLE
            ll_eta_etd.visibility = View.VISIBLE
            tv_eta.text = vehicleCheckOutDo.eta
            tv_ata.text = vehicleCheckOutDo.checkInDate!!.replace("/", "-")
            tv_atd.text = vehicleCheckOutDo.checkOutDate!!.replace("/", "-")
            tv_etd.text = vehicleCheckOutDo.etd
            tv_driver_name.text = vehicleCheckOutDo.site
            tv_vr_id.text = vehicleCheckOutDo.vrID
            tv_status.text = vehicleCheckOutDo.status
            tv_odo_start.text = vehicleCheckOutDo.startRead
            tv_odo_end.text = vehicleCheckOutDo.endRead
            tvStatus.text = vehicleCheckOutDo.status
            imv_tick_check_in.visibility = View.GONE
            hideLoader()
        } else {
            checkInInfo()
        }
    }

    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if (from.equals("CheckInSuccess")) {
            location(ResultListner { `object`, isSuccess ->
                if (isSuccess) {
                    hideLoader()
                    vehicleCheckInDo.checkoutLattitude = lattitudeFused
                    vehicleCheckInDo.checkoutLongitude = longitudeFused
//                    preferenceUtils.saveString(PreferenceUtils.OPENING_READING, tv_odo_start.text.toString())
//                    preferenceUtils.saveString(PreferenceUtils.CLOSING_READING, tv_odo_end.text.toString())

                    StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)

                    stopLocationUpdates()
                    vehicleCheckInDo.checkOutTimeCapturecheckOutTime = CalendarUtils.getTime()
                    vehicleCheckInDo.checkOutTimeCapturcheckOutDate = CalendarUtils.getDate()
                    checkOutTimeCapture()
                }
            })


//            val intent = Intent(this@DashBoardActivity, DashBoardActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent . FLAG_ACTIVITY_NEW_TASK)
//            startActivity(intent)

        } else if (from.equals("CHECKOUT")) {
            checkout()
        } else {
            hideLoader()
        }
    }

    private fun clearDataLocalData() {
        hideLoader()
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
        var vehicleCheckOutDo = VehicleCheckOutDo()

        vehicleCheckOutDo.checkInDate = tv_ata.text.toString()
        vehicleCheckOutDo.checkOutDate = CalendarUtils.getCurrentDate(this)
        vehicleCheckOutDo.eta = tv_eta.text.toString()
        vehicleCheckOutDo.etd = tv_etd.text.toString()
        vehicleCheckOutDo.site = tv_driver_name.text.toString()
        vehicleCheckOutDo.vrID = tv_vr_id.text.toString()
        vehicleCheckOutDo.status = tv_status.text.toString()
        vehicleCheckOutDo.startRead = tv_odo_start.text.toString()
        vehicleCheckOutDo.endRead = tv_odo_end.text.toString()
        vehicleCheckOutDo.checkoutFlag = "2"
        vehicleCheckOutDo.status = "Vehicle Checked-Out"

        StorageManager.getInstance(this).insertVehicleCheckOutData(this, vehicleCheckOutDo)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.START_ROUTE)
        preferenceUtils.removeFromPreference(PreferenceUtils.INSTRUCTIONS)
        preferenceUtils.removeFromPreference(PreferenceUtils.OPERATION)

        preferenceUtils.removeFromPreference(PreferenceUtils.ODO_READ)
        preferenceUtils.removeFromPreference(PreferenceUtils.ODO_UNIT)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES)
        preferenceUtils.removeFromPreference(PreferenceUtils.OPENING_READING)
        preferenceUtils.removeFromPreference(PreferenceUtils.ODO_UNIT)

        preferenceUtils.removeFromPreference(PreferenceUtils.CLOSING_READING)
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME)
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT)
        preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECK_IN_STATUS)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION)
        AppPrefs.clearPref(this)
        StorageManager.getInstance(this).deleteCheckInData(this)
        StorageManager.getInstance(this).deleteCompletedShipments(this)
        StorageManager.getInstance(this).deleteSkipShipmentList(this)
        StorageManager.getInstance(this).deleteVanNonScheduleProducts(this)
        StorageManager.getInstance(this).deleteVanScheduleProducts(this)
        StorageManager.getInstance(this).deleteSpotSalesCustomerList(this)
        StorageManager.getInstance(this).deleteVehicleInspectionList(this)
        StorageManager.getInstance(this).deleteGateInspectionList(this)
        StorageManager.getInstance(this).deleteSiteListData(this)
        StorageManager.getInstance(this).deleteShipmentListData(this)
        StorageManager.getInstance(this).deleteCheckOutData(this)
        StorageManager.getInstance(this).deleteSerialEquipmentSelectionDO(this)

        StorageManager.getInstance(this).deleteTrailerSelectionDO(this)
        StorageManager.getInstance(this).deleteEquipmentSelectionDO(this)

        imv_tick_check_in.visibility = View.GONE
        StorageManager.getInstance(this).deleteScheduledNonScheduledReturnData()// clearing all tables
        showToast(getString(R.string.updated_successfully))
        preferenceUtils.saveString(PreferenceUtils.STATUS, "" + vehicleCheckOutDo.status)
        preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, vehicleCheckOutDo.status)
        onResume()
        tvCheckedIn.setText("" + resources.getString(R.string.check_in))
        tv_status.text = vehicleCheckOutDo.status

    }

    private fun checkOutTimeCapture() {
        showLoader()
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()

        var vehicleCheckOutDo = StorageManager.getInstance(this).getVehicleCheckOutData(this)
        vehicleCheckOutDo.checkOutTimeCaptureArrivalTime = CalendarUtils.getDate()
        vehicleCheckOutDo.checkOutTimeCaptureArrivalDate = CalendarUtils.getTime()
        vehicleCheckOutDo.checkOutTimeCaptureStartLoadingTime = CalendarUtils.getTime()
        vehicleCheckOutDo.checkOutTimeCaptureStartLoadingDate = CalendarUtils.getDate()
        vehicleCheckOutDo.checkOutTimeCaptureStockLoadingTime = CalendarUtils.getTime()
        vehicleCheckOutDo.checkOutTimeCaptureStockLoadingDate = CalendarUtils.getDate()
        vehicleCheckOutDo.checkOutTimeCaptureEndLoadingTime = CalendarUtils.getTime()
        vehicleCheckOutDo.checkOutTimeCaptureEndLoadingDate = CalendarUtils.getDate()
        vehicleCheckOutDo.checkOutTimeCaptureVehicleInspectionTime = CalendarUtils.getTime()
        vehicleCheckOutDo.checkOutTimeCapturVehicleInspectionDate = CalendarUtils.getDate()
        vehicleCheckOutDo.checkOutTimeCaptureGateInspectionTime = CalendarUtils.getTime()
        vehicleCheckOutDo.checkOutTimeCapturGateInspectionDate = CalendarUtils.getDate()
        vehicleCheckOutDo.checkOutTimeCapturcheckOutDate = CalendarUtils.getDate()
        vehicleCheckOutDo.checkOutTimeCapturecheckOutTime = CalendarUtils.getTime()
        StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
        val request = CheckoutTimeCaptureRequest(driverId, date, this)
        request.setOnResultListener { isError, loginDo ->
            if (loginDo != null) {
                if (isError) {
                    hideLoader()
                    showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                } else {

                    if (loginDo.flag == 1) {
                        vrSuccess(2)
                    } else {
                        hideLoader()
                        showToast(getString(R.string.unable_to_check_out_try_again))
                    }
                }
            } else {
                hideLoader()
                showToast(getString(R.string.unable_to_check_out_try_again))

            }
        }

        request.execute()
    }

    private fun vrSuccess(type: Int) {
        if (type == 3 || type == 4) {
            val siteListRequest = DashboardOperationRequest(type, this)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                hideLoader()
            }
            siteListRequest.execute()
        } else {
            val siteListRequest = VRTimeCaptureRequest(type, this)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (type == 2) {
                    clearDataLocalData()

                }

            }
            siteListRequest.execute()
        }

    }

    private fun loadData() {
        showLoader()
        val vehicleRoutId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")

        if (vehicleRoutId.length > 0) {
            val driverListRequest = DisplayPickupListRequest(this, vehicleRoutId)
            driverListRequest.setOnResultListener { isError, pods ->
                hideLoader()
                if (isError) {

                } else {
                    pickUpDos = pods
                    if (pickUpDos.size > 0) {
                        for (k in pickUpDos.indices) {
                            if (pickUpDos.get(k).departuredFlag != 2) {
                                if (pickUpDos.get(k).cancelFlag == 30) {
                                    isDepartureExisted = false

                                } else {
                                    isDepartureExisted = true

                                }
                                break
                            } else {
                                isDepartureExisted = false
                            }
                        }
                        if (isDepartureExisted) {
                            showAppCompatAlert("", getString(R.string.kindly), getString(R.string.ok), getString(R.string.cancel), "CHECKOUT", true)

                        } else {
                            checkout()
                        }

                    } else {
                        var customDialog = Dialog(this);
                        customDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
                        customDialog.setContentView(R.layout.closing_reading_dialog);

                        customDialog.show();

                        var read = preferenceUtils.getStringFromPreference(PreferenceUtils.OPENING_READING, "")
                        customDialog.etOpeningReading.setText(read.toString())
                        if (read.isNotEmpty()) {
                            customDialog.etOpeningReading.isClickable = true
                            customDialog.etOpeningReading.isEnabled = true

                        } else {
                            customDialog.etOpeningReading.isClickable = false
                            customDialog.etOpeningReading.isEnabled = false

                        }
                        customDialog.button1.setOnClickListener {
                            customDialog.dismiss();
                        }
                        customDialog.button2.setOnClickListener {
                            customDialog.dismiss();
                            if (customDialog.etOpeningReading.text.toString().isNullOrEmpty()) {
                                showAlert(getString(R.string.open_msg))

                            } else if (customDialog.etClosingReading.text.toString().isNullOrEmpty()) {
                                showAlert(getString(R.string.close_msg))

                            } else {
                                var reading = customDialog.etOpeningReading.text.toString().toDouble();
                                var closereading = customDialog.etClosingReading.text.toString().toDouble();
                                if (closereading < reading) {
                                    showAlert(getString(R.string.closing_msg))
                                } else {
                                    preferenceUtils.saveString(PreferenceUtils.OPENING_READING, customDialog.etOpeningReading.text.toString())
                                    preferenceUtils.saveString(PreferenceUtils.CLOSING_READING, customDialog.etClosingReading.text.toString())

                                    var stringBuilder = StringBuilder()
                                    stringBuilder.append("CheckOut At ")
                                    stringBuilder.append(CalendarUtils.getCurrentDate(this))
                                    stringBuilder.append(" ?")
                                    showAppCompatAlert(getString(R.string.alert), "" + stringBuilder.toString(), getString(R.string.ok), getString(R.string.cancel), "CheckInSuccess", false)

                                }
                            }


                        }
                    }

                }
            }
            driverListRequest.execute()
        } else {
            var stringBuilder = StringBuilder()
            stringBuilder.append("CheckOut At ")
            stringBuilder.append(CalendarUtils.getCurrentDate(this))
            stringBuilder.append(" ?")
            showAppCompatAlert(getString(R.string.alert), "" + stringBuilder.toString(), getString(R.string.ok), getString(R.string.cancel), "CheckInSuccess", false)

        }

    }

    fun checkout() {
        var customDialog = Dialog(this);
        customDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        customDialog.setContentView(R.layout.closing_reading_dialog);

        customDialog.show();

        var read = preferenceUtils.getStringFromPreference(PreferenceUtils.OPENING_READING, "")
        if (read.isNullOrEmpty()) {
            customDialog.etOpeningReading.setText("0")

        }
        val unit = preferenceUtils.getStringFromPreference(PreferenceUtils.ODO_UNIT, "")
        customDialog.tvUnit2.setText("" + unit)
        customDialog.tvUnit3.setText("" + unit)

        customDialog.etOpeningReading.setText(read.toString())
        if (read == null || read.isEmpty()) {
            customDialog.etOpeningReading.isClickable = true
            customDialog.etOpeningReading.isEnabled = true

        } else {
            customDialog.etOpeningReading.isClickable = false
            customDialog.etOpeningReading.isEnabled = false

        }
        customDialog.button1.setOnClickListener {
            customDialog.dismiss();
        }
        customDialog.button2.setOnClickListener {
            customDialog.dismiss();
            if (customDialog.etOpeningReading.text.toString().isNullOrEmpty()) {
                showAlert(getString(R.string.open_msg))

            } else if (customDialog.etClosingReading.text.toString().isNullOrEmpty()) {
                showAlert(getString(R.string.close_msg))

            } else {
                var reading = customDialog.etOpeningReading.text.toString()
                var closereading = customDialog.etClosingReading.text.toString()
                if (closereading.toDouble() < reading.toDouble()) {
                    showAlert(getString(R.string.closing_msg))
                } else {
                    tv_odo_end.text = resources.getString(R.string.end_reading) + " : " + closereading + " " + unit

                    preferenceUtils.saveString(PreferenceUtils.OPENING_READING, customDialog.etOpeningReading.text.toString())
                    preferenceUtils.saveString(PreferenceUtils.CLOSING_READING, customDialog.etClosingReading.text.toString())
                    var stringBuilder = StringBuilder()
                    stringBuilder.append("CheckOut At ")
                    stringBuilder.append(CalendarUtils.getCurrentDate(this))
                    stringBuilder.append(" ?")
                    showAppCompatAlert(getString(R.string.alert), "" + stringBuilder.toString(), getString(R.string.ok), getString(R.string.cancel), "CheckInSuccess", false)

                }
            }


        }
    }

    fun checkinLoad() {

        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = CheckinStatusRequest(this)
            driverListRequest.setOnResultListener { isError, checkinDO ->
                //hideLoader()
                if (isError) {
                    hideLoader()

//                    showAlert(getString(R.string.server_error))
                } else {
                    if (checkinDO != null && checkinDO.status == 20) {
                        if (checkinDO.statusFlag == 3) {
                            preferenceUtils.saveString(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "" + checkinDO.vrID)
                            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, "" + checkinDO.vrID)

                            preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, "" + checkinDO.vrID)
                            preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, "" + checkinDO.transactionID)
                            preferenceUtils.saveString(PreferenceUtils.DOC_NUMBER, checkinDO.transactionID)
                            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, checkinDO.vehicleCode)
                            preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, checkinDO.vehicleCarrier)
                            preferenceUtils.saveString(PreferenceUtils.LOCATION, checkinDO.location)
                            preferenceUtils.saveString(PreferenceUtils.V_PLATE, checkinDO.plate)
                            preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, checkinDO.nShipments)
                            preferenceUtils.saveString(PreferenceUtils.LOCATION_TYPE, checkinDO.locationType)
                            preferenceUtils.saveString(PreferenceUtils.NON_VEHICLE_CODE, checkinDO.vehicleCode)
                            preferenceUtils.saveString(PreferenceUtils.CA_TIME, checkinDO.aTime)
                            preferenceUtils.saveString(PreferenceUtils.CA_DATE, checkinDO.aDate)
                            preferenceUtils.saveString(PreferenceUtils.CD_DATE, checkinDO.dDate)
                            preferenceUtils.saveString(PreferenceUtils.CD_TIME, checkinDO.dTime)
                            preferenceUtils.saveString(PreferenceUtils.ODO_UNIT, checkinDO.unit)
                            preferenceUtils.saveInt(PreferenceUtils.ODO_READ, checkinDO.odometer)
                            preferenceUtils.saveString(PreferenceUtils.OPENING_READING, checkinDO.odometer.toString())
                            preferenceUtils.saveString(PreferenceUtils.OPENING_READING, checkinDO.startRead.toString())
                            preferenceUtils.saveString(PreferenceUtils.CLOSING_READING, checkinDO.endRead.toString())

                            vehicleCheckInDo.actualvehicleCheckin = checkinDO.actualCheckin
                            vehicleCheckInDo.actualvehicleCheckout = checkinDO.actualCheckout
                            vehicleCheckInDo.statusflag = checkinDO.statusFlag.toString()

                            AppPrefs.putString(AppPrefs.CA_TIME, checkinDO.aTime)
                            AppPrefs.putString(AppPrefs.CA_DATE, checkinDO.aDate)
                            AppPrefs.putString(AppPrefs.CD_DATE, checkinDO.dDate)
                            AppPrefs.putString(AppPrefs.CD_TIME, checkinDO.dTime)
                            vehicleCheckInDo.loadStock = resources.getString(R.string.checkin_vehicle)
                            AppPrefs.putBoolean(AppPrefs.STOCK_CONFIRMED, true)
                            AppPrefs.putBoolean(AppPrefs.CHECK_IN_DONE, true)


                            preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE, CalendarUtils.getCurrentDate(this))


                            preferenceUtils.saveString(PreferenceUtils.STATUS, "" + resources.getString(R.string.checkin_vehicle))
                            preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, resources.getString(R.string.checkin_vehicle))
                            vehicleCheckInDo.checkInStatus = resources.getString(R.string.checkin_vehicle)//resources.getString(R.string.checkin_vehicle)
                            vehicleCheckInDo.checkInTime = CalendarUtils.getCurrentDate(this)
                            vehicleCheckInDo.checkinTimeCaptureCheckinTime = CalendarUtils.getTime()
                            vehicleCheckInDo.checkinTimeCapturCheckinDate = CalendarUtils.getDate()
                            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)

                            hideLoader()
                            onResume()
                        } else {
                            var vehicleCheckOutDo = VehicleCheckOutDo()

                            if (!TextUtils.isEmpty(checkinDO.aDate)) {
                                val dMonth: String = checkinDO.aDate.substring(4, 6)
                                val dyear: String = checkinDO.aDate.substring(0, 4)
                                val dDate: String = checkinDO.aDate.substring(Math.max(checkinDO.aDate.length - 2, 0))
                                val time1: String = checkinDO.aTime.substring(0, 2)
                                val time2: String = checkinDO.aTime.substring(2, 4)


                                vehicleCheckOutDo.eta = dMonth + "-" + dDate + "-" + dyear + "  " + time1 + ":" + time2


                            }

                            if (!TextUtils.isEmpty(checkinDO.dDate) && !TextUtils.isEmpty(checkinDO.dTime)) {
                                tv_etd.visibility = View.VISIBLE

                                val dMonth: String = checkinDO.dDate.substring(4, 6)
                                val dyear: String = checkinDO.dDate.substring(0, 4)
                                val dDate: String = checkinDO.dDate.substring(Math.max(checkinDO.dDate.length - 2, 0))
                                val time1: String = checkinDO.dTime.substring(0, 2)
                                val time2: String = checkinDO.dTime.substring(2, 4)

                                vehicleCheckOutDo.etd = dMonth + "-" + dDate + "-" + dyear + "  " + time1 + ":" + time2
                            } else {
                                tv_etd.visibility = View.GONE
                            }

                            vehicleCheckOutDo.checkInDate = checkinDO.actualCheckin
                            vehicleCheckOutDo.checkOutDate = checkinDO.actualCheckout
                            vehicleCheckOutDo.site = checkinDO.siteDescription
                            vehicleCheckOutDo.vrID = checkinDO.vrID
                            vehicleCheckOutDo.status = "Vehicle Checked-Out"
                            vehicleCheckOutDo.startRead = getString(R.string.startreadlabel) + " :  " + checkinDO.startRead.toString() + " " + checkinDO.unit
                            vehicleCheckOutDo.endRead = getString(R.string.endreadlabel) + " :  " + checkinDO.endRead.toString() + " " + checkinDO.unit
                            vehicleCheckOutDo.checkoutFlag = "2"
                            StorageManager.getInstance(this).insertVehicleCheckOutData(this, vehicleCheckOutDo)
                            imv_tick_check_in.visibility = View.GONE
                            hideLoader()
                            onResume()
                        }


                    } else {
//                        showToast(getString(R.string.no_data_found))
//                        tvNoData.visibility = View.VISIBLE
                        hideLoader()
                    }

                }
            }
            driverListRequest.execute()
        } else {
            YesOrNoDialogFragment().newInstance(getString(R.string.alert), getString(R.string.internet_connection), ResultListner { `object`, isSuccess -> }, false).show((this as AppCompatActivity).supportFragmentManager, "YesOrNoDialogFragment")
        }

    }

    private fun showInstructions() {
        val dialog = Dialog(this)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.instruction_vr_dialog)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        dialog.window!!.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val tvInstruction = dialog.findViewById<TextView>(R.id.tvInstruction)
        val tvClose = dialog.findViewById<TextView>(R.id.tvClose)
        tvInstruction.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.INSTRUCTIONS, ""))

        tvClose.setOnClickListener {
            dialog.dismiss()


        }
        dialog.show()
    }

}