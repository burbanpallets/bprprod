package com.tbs.generic.vansales.Activitys

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.database.StorageManager
import com.bumptech.glide.Glide


//
class ViewScannedImageActivity : BaseActivity() {
    var ivSign: ImageView? = null

    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.alerts_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.text = getString(R.string.view_image)
        ivSign = findViewById<View>(R.id.ivSign) as ImageView
        var podDo = StorageManager.getInstance(this).getDepartureData(this)
        val path = podDo.signature
        val bmp = BitmapFactory.decodeFile(path)

        Glide.with(this)
                .load<Any>(path)
                .into(ivSign)
    }


}