package com.tbs.generic.vansales.Activitys;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.ActivityCompat;

import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.Requests.CustomersLocationRequest;
import com.tbs.generic.vansales.Requests.UpdateLocationRequest;
import com.tbs.generic.vansales.common.WorkaroundMapFragment;
import com.tbs.generic.vansales.utils.PreferenceUtils;


import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 30-Mar-18.
 */

public class CustomerLocActivty extends BaseActivity implements OnMapReadyCallback, LocationListener {

    private RelativeLayout llAboutUs;
    private Button btnSubmit;

    private WorkaroundMapFragment mapFragment;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private GoogleMap mMap;
    private LatLng latLng;
    private LocationManager locationManager;
    private String customercode,addreeCode,telephone,email,mobile,postalcode;
    int type;
    String addressLine1 = "";
    String addressLine2 = "";
    String addressLine3 ="";
    String cityName = "";
    @Override
    public void initializeControls() {
        btnSubmit = llAboutUs.findViewById(R.id.btnConfirm);
        if (getIntent().hasExtra("C_CODE")) {
            customercode = getIntent().getExtras().getString("C_CODE");
        }
        if (getIntent().hasExtra("A_CODE")) {
            addreeCode = getIntent().getExtras().getString("A_CODE");
        }
        if (getIntent().hasExtra("TELEPHONE")) {
            telephone = getIntent().getExtras().getString("TELEPHONE");
        }
        if (getIntent().hasExtra("EMAIL")) {
            email = getIntent().getExtras().getString("EMAIL");
        }
        if (getIntent().hasExtra("MOBILE")) {
            mobile = getIntent().getExtras().getString("MOBILE");
        }
        if (getIntent().hasExtra("TYPE")) {
            type = getIntent().getExtras().getInt("TYPE");
        }
        tvScreenTitle.setText(R.string.customer_location);

    }

    @Override
    public void initialize() {
        llAboutUs = (RelativeLayout) getLayoutInflater().inflate(R.layout.customer_location, null);
        llBody.addView(llAboutUs, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        changeLocale();
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestSingleUpdate(criteria, CustomerLocActivty.this, Looper.getMainLooper());
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, CustomerLocActivty.this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, CustomerLocActivty.this);
        configureCameraIdle();
        mapFragment = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//            mapFragment.getMapAsync(this);
//        } else {
//            // Show rationale and request permission.
//        }
        mapFragment.getMapAsync(this);

        mapFragment.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                llAboutUs.requestDisallowInterceptTouchEvent(true);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( latLng!=null){
                    captureCustomerLocationRequest(customercode,addreeCode,type, latLng.latitude, latLng.longitude,CustomerLocActivty.this);

                }

            }
        });


    }


    private void captureCustomerLocationRequest(String cusCode,String addresscode,int type, Double lat, Double lng, Context context) {
        UpdateLocationRequest driverListRequest = new UpdateLocationRequest(telephone,email,mobile,addressLine1,addressLine2,
                addressLine3,cityName,postalcode,cusCode,addresscode,type, lat, lng, context);
        showLoader();
        driverListRequest.setOnResultListener(new UpdateLocationRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, SuccessDO customerDetailsMainDo) {
                if (isError) {
                    hideLoader();

                    showToast("Location not Updated");
                } else {
                    hideLoader();

                            if (customerDetailsMainDo.flag == 2) {
                                showToast("Location Updated Successfully...");
                                finish();


                            } else {
                                showToast("Location not Updated");
                                finish();

                            }




                }
            }

        });
        driverListRequest.execute();

    }

    private void configureCameraIdle() {
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                try {
                    latLng = mMap.getCameraPosition().target;
                    Geocoder geocoder = new Geocoder(CustomerLocActivty.this);
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                         addressLine1 = addressList.get(0).getAddressLine(0);
                         addressLine2 = addressList.get(0).getAddressLine(1);
                         addressLine3 = addressList.get(0).getAddressLine(2);
                         cityName = addressList.get(0).getLocality();
                        postalcode = addressList.get(0).getPostalCode();
//                        getAddressFromLocation(latLng.latitude, latLng.longitude,
//                                getApplicationContext(), new GeocoderHandler());

//                        String country = addressList.get(0).getCountryName();
//                        if (!locality.isEmpty() && !country.isEmpty()) {
////                            captureCustomerLocationRequest(code, latLng.latitude, latLng.longitude, getApplicationContext());
////                            showAppCompatAlert("Info !", ""+locality+""+ country, "Ok", "", "", false);
//
////                            showAlert(""+locality+""+ country);
//                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }


    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setRotateGesturesEnabled(true);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.setOnCameraIdleListener(onCameraIdleListener);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onLocationChanged(Location location) {
        Double lat = 0.0;
        Double lng = 0.0;
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        String latt = preferenceUtils.getStringFromPreference(PreferenceUtils.C_LAT, "");
        String lngg = preferenceUtils.getStringFromPreference(PreferenceUtils.C_LONG, "");


        if (latt.length() > 0) {

            lat = Double.valueOf(latt);
        }
        if (lngg.length() > 0) {
            lng = Double.valueOf(lngg);

        }


        if (mMap!=null&&latLng!=null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));

        } else {
            LatLng point = new LatLng(lat, lng);
           if(mMap!=null) {
               mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 15.0f));

           }

//            drawMarker(point);


        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
//            btnSubmit.setText(locationAddress);
        }
    }
}
