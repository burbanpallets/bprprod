package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class ConfirmationQtyRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String id;
    String username,password,ip,pool,port;
    private String shipmentID;
    PreferenceUtils preferenceUtils;
    private ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
    public ConfirmationQtyRequest(String shipment, ArrayList<ActiveDeliveryDO> activeDeliveryDoS, Context mContext) {
        this.mContext = mContext;

        this.activeDeliveryDOS = activeDeliveryDoS;
        this.shipmentID = shipment;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO successDO);

    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_YSDHNUM", shipmentID);
            JSONArray jsonArray =new JSONArray();


            if(activeDeliveryDOS!=null && activeDeliveryDOS.size()>0) {
                for (int i = 0; i < activeDeliveryDOS.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_YITMREF", activeDeliveryDOS.get(i).product);
                    jsonObject1.put("I_YQTY", activeDeliveryDOS.get(i).orderedQuantity);
                    jsonObject1.put("I_YSDDLIN", activeDeliveryDOS.get(i).line);


                 /*   jsonArray.put("I_YQTY",createDeliveryDoS.get(i).totalQuantity);
                    jsonArray.put("I_YITMREF",createDeliveryDoS.get(i).item);*/
                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }




        } catch (Exception e) {
            System.out.println("Exception " + e);
        }

        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CONFIRM_QTY, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    }
                    else if (startTag.equalsIgnoreCase("GRP")) {

                    }
                    else if (startTag.equalsIgnoreCase("TAB")) {
                    //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    }else if (startTag.equalsIgnoreCase("LIN")) {
                  //      createPaymentDO = new CustomerDetailsDo();

                    }
                }
                else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if(text.length()>0){
                                successDO.flag= Integer.parseInt(text);

                            }


                        }
                    }


                     if (endTag.equalsIgnoreCase("GRP")) {
                       // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                       // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity)mContext).showLoader();

       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity)mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }
}