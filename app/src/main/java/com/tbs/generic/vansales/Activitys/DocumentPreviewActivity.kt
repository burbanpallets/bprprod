package com.tbs.generic.vansales.Activitys

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.itextpdf.text.Phrase
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import kotlinx.android.synthetic.main.document_preview.*


//
class DocumentPreviewActivity : BaseActivity() {
    lateinit var activeDeliveryMainDO: ActiveDeliveryMainDO
    var docType = 0
    var type = 0;

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.document_preview, null) as ConstraintLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()

    }

    override fun initializeControls() {

        if (intent.hasExtra("TYPE")) {
            type = intent.extras?.getInt("TYPE")!!
        }
        prepareDeliveryNoteCreation()
    }

    private fun getAddress(): String? {
        val street: String = activeDeliveryMainDO.customerStreet
        val landMark: String = activeDeliveryMainDO.customerLandMark
        val town: String = activeDeliveryMainDO.customerTown
        val city: String = activeDeliveryMainDO.customerCity
        val postal: String = activeDeliveryMainDO.customerPostalCode
        val countryName: String = activeDeliveryMainDO.countryName
        var finalString = ""
        if (!TextUtils.isEmpty(street)) {
            finalString += "$street, "
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += "$landMark, "
        }
        tvScreenTitle.setText(resources.getString(R.string.preview))
        if (!TextUtils.isEmpty(town)) {
            finalString += "$town, "
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += "$city, "
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += "$postal, "
        }
        //        if (!TextUtils.isEmpty(countryName)) {
//            finalString += countryName;
//        }
        return finalString
    }

    private fun prepareDeliveryNoteCreation() {
        var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        docType = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);
        var id = ""
        if (type == 1) {
            id = activeDeliverySavedDo.shipmentNumber

        } else if (type == 2) {
            id = activeDeliverySavedDo.loandelivery
            docType=1
        } else if (type == 3) {
            id = preferenceUtils.getStringFromPreference(PreferenceUtils.LOAN_RETURN, "")
        } else {
            id = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

        }



        if (id.length > 0) {
            showLoader()

            val driverListRequest = ActiveDeliveryRequest(docType, id, this@DocumentPreviewActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                if (activeDeliveryDo != null) {
                    if (isError) {
                        hideLoader()
                        showAppCompatAlert(getString(R.string.error), getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                    } else {
                        hideLoader()
                        activeDeliveryMainDO = activeDeliveryDo
                        if (activeDeliveryDo.activeDeliveryDOS.size > 0) {

//                            receiverNum.setText(activeDeliveryDo.capturedNumber)
//                            recieverName.setText(activeDeliveryDo.capturedName)
//                            paymentTerm.setText(activeDeliveryDo.paymentTerm)

                            for (i in 0 until activeDeliveryDo.activeDeliveryDOS.size) {
                                val view: View = layoutInflater.inflate(R.layout.content_row, null)
                                val item_name = view.findViewById<View>(R.id.productDescription) as TextView
                                val sno = view.findViewById<View>(R.id.sno) as TextView

                                val quantity = view.findViewById<View>(R.id.qty) as TextView
                                var s = i + 1
                                sno.setText("" + s)

                                item_name.setText(activeDeliveryDo.activeDeliveryDOS.get(i).productDescription)
                                quantity.setText("" + activeDeliveryDo.activeDeliveryDOS.get(i).orderedQuantity + " " + activeDeliveryDo.activeDeliveryDOS.get(i).unit)

                                content.addView(view)
                            }
//                             val image = PDFOperations.getInstance().getSignBitmap(activeDeliveryDo.signature)


                        }else{
                            llProducts.visibility=View.GONE
                        }
                        documenTNum.setText(activeDeliveryDo.shipmentNumber)
                        deliveredTo.setText(activeDeliveryDo.customerDescription)
                        userID.setText(activeDeliveryDo.createUserID)
                        user_name.setText(activeDeliveryDo.createUserName)
                        if (activeDeliveryDo.createdDate != null && activeDeliveryDo.createdDate.length > 0) {
                            val dMonth = activeDeliveryDo.createdDate.substring(4, 6)
                            val dyear = activeDeliveryDo.createdDate.substring(0, 4)
                            val dDate = activeDeliveryDo.createdDate.substring(Math.max(activeDeliveryDo.createdDate.length - 2, 0))
                            date.setText(dDate + "-" + dMonth + "-" + dyear)
                        }
                        time.setText(activeDeliveryDo.createdTime)
                        registered_supplier_address.setText(getAddress())
                        customerCode.setText(activeDeliveryDo.customer)
                        if(activeDeliveryDo.paymentTerm.isNotEmpty()){
                            llPT.visibility=View.VISIBLE
                            paymentTerm.setText(activeDeliveryDo.paymentTerm)

                        }

                        recieverName.setText(activeDeliveryDo.customerDescription)
                        receiverNum.setText(activeDeliveryDo.mobile)

                        var vehiclCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

                        vehicleCode.setText(vehiclCode)
                        namePlate.setText(activeDeliveryDo.customer)
                        if (!activeDeliveryDo.signature.isNullOrEmpty()) {
                            val decodedString: ByteArray = android.util.Base64.decode(activeDeliveryDo.signature, android.util.Base64.DEFAULT)
                            val decodedByte: Bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                            img_qr_code_image.setImageBitmap(decodedByte)
                            img_qr_code_image.visibility = View.VISIBLE

                        }
                        progressBarCode.visibility = View.GONE
                    }
                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)

                }
            }
            driverListRequest.execute()
        } else {
            showAlert(resources.getString(R.string.no_data_found))
        }
    }

}