package com.tbs.generic.vansales.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.Model.NonScheduledProductMainDO
import com.tbs.generic.vansales.Model.ProductDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.Util

//
class SalesCaptureDeliveryActivity : BaseActivity() {
    lateinit var productDOS: List<ProductDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnCreateDelivery: Button
lateinit var nonScheduledProductMainDO: NonScheduledProductMainDO
    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.sales_capture_delivery, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.capture_delivery_details)

        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        val btnScan = findViewById<Button>(R.id.btnScan)
        btnScan.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this@SalesCaptureDeliveryActivity, ScanProductsActivity::class.java)
            startActivity(intent)
        }
        ivAdd.visibility = View.VISIBLE
       ivAdd.setOnClickListener{
           Util.preventTwoClick(it)
            val intent = Intent(this, AddProductActivity::class.java)

           startActivity(intent)
       }
        btnCreateDelivery = findViewById<Button>(R.id.btnCreateDelivery)

        btnCreateDelivery.setOnClickListener {


        }
    }

}