package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class PdfInvoiceDo implements Serializable {


    public String includingTax = "";
    public String excludingTax = "";
    public String product = "";
    public String productDesccription = "";
    public String deliveredQunatity = "";
    public String grossPrice = "";
    public String netPrice = "";
    public Double costPrice =0.0;

    public String amount              = "";
    public String discount            = "";
    public Double vatPerAmount = 0.0;
    public Double vatPercentage=0.0;
    public String quantityUnits        = "";


    @Override
    public String toString() {
        return "PdfInvoiceDo{" +
                "includingTax='" + includingTax + '\'' +
                ", excludingTax='" + excludingTax + '\'' +
                ", product='" + product + '\'' +
                ", productDesccription='" + productDesccription + '\'' +
                ", deliveredQunatity='" + deliveredQunatity + '\'' +
                ", grossPrice='" + grossPrice + '\'' +
                ", netPrice='" + netPrice + '\'' +
                ", costPrice=" + costPrice +
                ", amount='" + amount + '\'' +
                ", discount='" + discount + '\'' +
                ", vatPerAmount=" + vatPerAmount +
                ", vatPercentage=" + vatPercentage +
                '}';
    }
}
