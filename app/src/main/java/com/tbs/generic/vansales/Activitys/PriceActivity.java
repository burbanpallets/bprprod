//package com.tbs.generic.vansales.Activitys;
//
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.tbs.generic.vansales.Adapters.PriceAdapter;
//import com.tbs.generic.vansales.Model.PriceDO;
//import com.tbs.generic.vansales.R;
//import com.tbs.generic.vansales.utils.Util;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class PriceActivity extends BaseActivity {
//
//    private RecyclerView recycleview;
//    private PriceAdapter priceAdapter;
//    private LinearLayout llOrderHistory;
//    private TextView tvScreenTitle, tvNoPrice;
//    private List<PriceDO> priceDOS;
//    private LinearLayout llSearch;
//    private EditText etSearch;
//    private ImageView ivClearSearch, ivSearch, ivGoBack;
//
//    @Override
//    public void initialize() {
//        llOrderHistory = (LinearLayout) getLayoutInflater().inflate(R.layout.site_screen, null);
//        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        changeLocale();
//        toolbar.setNavigationIcon(R.drawable.back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        disableMenuWithBackButton();
//        initializeControls();
//        tvScreenTitle.setVisibility(View.VISIBLE);
//        tvScreenTitle.setText(R.string.price_list);
//        recycleview.setLayoutManager(new LinearLayoutManager(this));
//
//        ivGoBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Util.preventTwoClick(v);
//                finish();
//            }
//        });
//        ivSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if(llSearch.visibility == View.VISIBLE){
////                llSearch.visibility = View.INVISIBLE
////            }
////            else{
////            }
//                tvScreenTitle.setVisibility(View.GONE);
//                llSearch.setVisibility(View.VISIBLE);
//            }
//        });
//
//        ivClearSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                etSearch.setText("");
//                if(priceDOS!=null &&priceDOS.size()>0){
//                    priceAdapter = new PriceAdapter(PriceActivity.this, priceDOS);
//                    recycleview.setAdapter(priceAdapter);
//                    tvNoPrice.setVisibility(View.GONE);
//                    recycleview.setVisibility(View.VISIBLE);
//                }
//                else{
//                    tvNoPrice.setVisibility(View.VISIBLE);
//                    recycleview.setVisibility(View.GONE);
//                }
//
//            }
//        });
//
//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if(etSearch.getText().toString().trim().equalsIgnoreCase("")){
//                    if(priceDOS!=null &&priceDOS.size()>0){
//                        priceAdapter = new PriceAdapter(PriceActivity.this,priceDOS);
//                        recycleview.setAdapter(priceAdapter);
//                        tvNoPrice.setVisibility(View.GONE);
//                        recycleview.setVisibility(View.VISIBLE);
//                    }
//                    else{
//                        tvNoPrice.setVisibility(View.VISIBLE);
//                        recycleview.setVisibility(View.GONE);
//                    }
//                }
//                else if(etSearch.getText().toString().trim().length()>2){
//                    filter(etSearch.getText().toString().trim());
//                }
//            }
//        });
//
//        showLoader();
//        PriceListRequest priceListRequest = new PriceListRequest(PriceActivity.this);
//        priceListRequest.setOnResultListener(new PriceListRequest.OnResultListener(){
//            @Override
//            public void onCompleted(boolean isError, List<PriceDO> priceDoS) {
//                hideLoader();
//                priceDOS =priceDoS;
//
//                if (isError) {
//                    Toast.makeText(PriceActivity.this, R.string.error_price_list, Toast.LENGTH_SHORT).show();
//                } else {
//                    if(priceDOS.size()>0){
//                        priceAdapter = new PriceAdapter(PriceActivity.this,priceDOS);
//                        recycleview.setAdapter(priceAdapter);
//                        tvNoPrice.setVisibility(View.GONE);
//                        recycleview.setVisibility(View.VISIBLE);
//                    }
//                    else{
//                        tvNoPrice.setVisibility(View.VISIBLE);
//                        recycleview.setVisibility(View.GONE);
//                        showAlert(""+R.string.error_NoData);
//                    }
//
//
//                }
//            }
//        });
//
//        priceListRequest.execute();
//        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);
//
//    }
//
//    @Override
//    public void initializeControls() {
//
//        recycleview             = findViewById(R.id.recycleview);
//        tvNoPrice               = findViewById(R.id.tvNoOrders);
//        tvScreenTitle           = findViewById(R.id.tvScreenTitle);
//        llSearch                = findViewById(R.id.llSearch);
//        etSearch                = findViewById(R.id.etSearch);
//        ivClearSearch           = findViewById(R.id.ivClearSearch);
//        ivGoBack                = findViewById(R.id.ivGoBack);
//        ivSearch                = findViewById(R.id.ivSearchs);
//    }
//
//    private ArrayList<PriceDO> filter(String filtered) {
//        ArrayList<PriceDO> priceDOs = new ArrayList();
//        for (int i=0; i<priceDOS.size(); i++){
//            if(priceDOS.get(i).siteName.contains(filtered) || priceDOS.get(i).siteId.contains(filtered)){
//                priceDOs.add(priceDOS.get(i));
//            }
//        }
//        if(priceDOs.size()>0){
//            priceAdapter.refreshAdapter(priceDOs);
//            tvNoPrice.setVisibility(View.GONE);
//            recycleview.setVisibility(View.VISIBLE);
//        }
//        else{
//            tvNoPrice.setVisibility(View.VISIBLE);
//            recycleview.setVisibility(View.GONE);
//        }
//        return priceDOs;
//    }
//}
