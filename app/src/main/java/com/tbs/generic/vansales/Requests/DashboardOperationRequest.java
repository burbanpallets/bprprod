package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.DashboardCaptureDO;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.Model.VehicleCheckInDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;

public class DashboardOperationRequest extends AsyncTask<String, Void, Boolean> {
    private SuccessDO successDO;
    private Context mContext;
    PreferenceUtils preferenceUtils;
    private int type;

    public DashboardOperationRequest(int type, Context mContext) {

        this.mContext = mContext;
        this.type = type;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO createInvoiceDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String vr_number = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "");
        DashboardCaptureDO vehicleCheckInDo = StorageManager.getInstance(mContext).getDashboardData(mContext);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_YSTA", type);
            jsonObject.put("I_YNUMPC", vr_number);
            jsonObject.put("I_YDATE", vehicleCheckInDo.getDate());
            jsonObject.put("I_YTIME", vehicleCheckInDo.getTime());
            jsonObject.put("I_YLAT", vehicleCheckInDo.getLattitude());
            jsonObject.put("I_YLON", vehicleCheckInDo.getLongitude());

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.VR_TIME_CAPTURE, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        } else if (attribute.equalsIgnoreCase("O_XAPPVERSION")) {
                            if (text.length() > 0) {

                                successDO.version = text;
                            }


                        }
                    }

                    text = "";
                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();


        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }


}