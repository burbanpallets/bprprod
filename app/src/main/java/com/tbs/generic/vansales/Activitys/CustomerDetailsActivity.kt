package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Model.CustomerDetailsMainDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.CustomerDetailsRequest
import com.tbs.generic.vansales.Requests.CustomersLocationRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.customer_details.*
import kotlinx.android.synthetic.main.customer_dialog.*
import kotlinx.android.synthetic.main.reading_dialog.button1
import kotlinx.android.synthetic.main.reading_dialog.button2

class CustomerDetailsActivity : BaseActivity(), AdapterView.OnItemClickListener, LocationListener {
    private var llOrderDetails: ScrollView? = null
    private var currency: TextView? = null
    private val locationManager: LocationManager? = null
    private var type = 0
    private var lat: Double? = null
    private var lng: Double? = null
    var addresscode: String? = null
    var customerCode: String? = null
    lateinit var customerDetailsMainDO:CustomerDetailsMainDo;
    override fun onLocationChanged(location: Location) {
        if (location != null) {
            lat = location.latitude
            lng = location.longitude
            val shipmentType = preferenceUtils!!.getStringFromPreference(PreferenceUtils.ShipmentType, "")
            if (shipmentType == resources.getString(R.string.checkin_scheduled)) {
                val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
                captureCustomerLocationRequest(activeDeliverySavedDo.customer, lat!!, lng!!, this)
            } else {
                val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                captureCustomerLocationRequest(customerDo.customer, lat!!, lng!!, this)
            }
            locationManager!!.removeUpdates(this)
        }
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String) {}
    override fun initialize() {
        var llOrderDetails = layoutInflater.inflate(R.layout.customer_details, null) as LinearLayout
        llBody.addView(llOrderDetails, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        // llshoppingCartLayout.setVisibility(View.GONE);
        if (intent.hasExtra("C_CODE")) {
            customerCode = intent.extras!!.getString("C_CODE")
        }
        if (intent.hasExtra("A_CODE")) {
            addresscode = intent.extras!!.getString("A_CODE")
        }
        if (intent.hasExtra("TYPE")) {
            type = intent.extras!!.getInt("TYPE")
        }

        ivCustomerLocation.visibility = View.GONE
        ivCustomerLocation.setOnClickListener {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT)
            } else {
                val popup = PopupMenu(applicationContext, ivCustomerLocation)
                popup.menuInflater.inflate(R.menu.location_menu, popup.menu)
                popup.setOnMenuItemClickListener { item ->
                    if (item.title == getString(R.string.customer_location)) {
                        //   captureCustomerLocation();
                        val intent = Intent(this@CustomerDetailsActivity, CustomerLocActivty::class.java)
                        intent.putExtra("C_CODE", customerCode)
                        intent.putExtra("A_CODE", addresscode)
                        intent.putExtra("TYPE", type)

                        startActivity(intent)
                    }
                    true
                }
                popup.show()
            }
        }
        btnLocation.setOnClickListener {
            val intent = Intent(this@CustomerDetailsActivity, CustomerLocActivty::class.java)
            intent.putExtra("C_CODE", customerCode)
            intent.putExtra("A_CODE", addresscode)
            intent.putExtra("TYPE", type)
            intent.putExtra("TELEPHONE", tvTelNumber.text.toString())
            intent.putExtra("MOBILE", tvMobileNumber.text.toString())
            intent.putExtra("EMAIL", tvEmail.text.toString())

            startActivity(intent)
            finish()
        }
//        tvPostalCode.setOnClickListener {
//            update(1)
//
//        }
        tvTelNumber.setOnClickListener {
            update(1)

        }
        tvMobileNumber.setOnClickListener {
            update(2)

        }
        tvEmail.setOnClickListener {
            update(3)

        }
//        tvCity.setOnClickListener {
//            update(5)
//
//        }

    }

    fun update(type: Int) {

        var customDialog = Dialog(this);
        customDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        customDialog.setContentView(R.layout.customer_dialog);
        customDialog.show();
        if (type == 1) {
            customDialog.tvUpdate.setText("Telephone")
            customDialog.etValue.setText(""+customerDetailsMainDO.telephone)
        } else if (type == 2) {
            customDialog.tvUpdate.setText("Mobile")
            customDialog.etValue.setText(""+customerDetailsMainDO.mobile)
        } else if (type == 3) {
            customDialog.tvUpdate.setText("Email")
            customDialog.etValue.setInputType(InputType.TYPE_CLASS_TEXT)

            customDialog.etValue.setText(""+customerDetailsMainDO.email)
        }



        customDialog.button1.setOnClickListener {
            customDialog.dismiss();
        }
        customDialog.button2.setOnClickListener {
            customDialog.dismiss();
            if (customDialog.etValue.text.toString().isNotEmpty()) {
                if(type==1){
                    tvTelNumber.setText(customDialog.etValue.text.toString())
                }
                if(type==2){
                    tvMobileNumber.setText(customDialog.etValue.text.toString())
                }
                if(type==3){
                    tvEmail.setText(customDialog.etValue.text.toString())
                }
                showToast(getString(R.string.updated_successfully))

            }

//                        if(reading!=null&&reading.isNaN())
//            onResume()
        }

    }

    private val data: Unit
        private get() {
            preferenceUtils = PreferenceUtils(this@CustomerDetailsActivity)
            val driverListRequest = CustomerDetailsRequest(customerCode, addresscode, type, this@CustomerDetailsActivity)
            driverListRequest.setOnResultListener { isError, customerDetailsMainDo ->
                hideLoader()
                if (isError) {
                    Toast.makeText(this@CustomerDetailsActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                } else {
                    customerDetailsMainDO=   customerDetailsMainDo
                    tvName.setText(customerDetailsMainDo.descrption)
                    tvCarrier.setText(customerDetailsMainDo.carrier)
                    tvLicence.setText(customerDetailsMainDo.licence)
                    tvTax.setText(customerDetailsMainDo.tax)
                    tvPostalCode.setText(customerDetailsMainDo.postalcode)
                    tvMobileNumber.setText(customerDetailsMainDo.mobile)
                    tvTelNumber.setText(customerDetailsMainDo.telephone)
                    tvEmail.setText(customerDetailsMainDo.email)
                    tvCity.setText(customerDetailsMainDo.city)

                }
            }
            driverListRequest.execute()
        }

    override fun initializeControls() {
        tvScreenTitle.text = getString(R.string.details)

        //        tvName = (TextView) findViewById(R.id.tvItemName);
//        tvDescription = (TextView) findViewById(R.id.tvItemDescription);
//        rule = (TextView) findViewById(R.id.rule);
//        term = (TextView) findViewById(R.id.term);
//        address = (TextView) findViewById(R.id.address);
//        bill = (TextView) findViewById(R.id.bill);
//        payCustmer = (TextView) findViewById(R.id.payCustmer);
//        code = (TextView) findViewById(R.id.code);
//        billAddress = (TextView) findViewById(R.id.billAddress);
//
//        payByAddress = (TextView) findViewById(R.id.payByAddress);
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {}
    private fun captureCustomerLocationRequest(id: String, lat: Double, lng: Double, context: Context) {
        if (Util.isNetworkAvailable(this)) {
            val driverListRequest = CustomersLocationRequest(id, lat, lng, context)
            driverListRequest.setOnResultListener { isError, customerDetailsMainDo ->
                if (isError) {
                    Toast.makeText(this@CustomerDetailsActivity, R.string.error_details_list, Toast.LENGTH_SHORT).show()
                } else {
                    hideLoader()
                    if (customerDetailsMainDo != null) {
                        if (isError) {
                            showToast(getString(R.string.failure))
                        } else {
                            if (customerDetailsMainDo.flag == 2) {
                                showToast("Location Updated")
                            } else {
                                showToast("customerDetailsMainDo")
                            }
                        }
                    }
                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)
        }
    }

    override fun onResume() {
        super.onResume()
        if (Util.isNetworkAvailable(this)) {
            data
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "FAILURE", false)
        }
    }
}