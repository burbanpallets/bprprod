package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.DeliveryDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class DeliveryAdapter extends RecyclerView.Adapter<DeliveryAdapter.MyViewHolder>{
    int count=0;
    private ArrayList<DeliveryDO> deliveryDOS;
    private String imageURL;
    private Context context;
    private PreferenceUtils preferenceUtils;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription,tvNumber;
        public RelativeLayout rlRemove,rlAdd;
        public ImageView ivRemove,ivAdd;

        public MyViewHolder(View view) {
            super(view);
            tvProductName   = view.findViewById(R.id.tvName);
            tvDescription   = view.findViewById(R.id.tvDescription);

        }
    }


    public DeliveryAdapter(Context context, ArrayList<DeliveryDO> deliveryDOS) {
        this.context = context;
        this.deliveryDOS = deliveryDOS;

        preferenceUtils = new PreferenceUtils(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delivery_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        final LoadStockDO loadStockDO =loadStockDOS.get(position);
//        holder.tvProductName.setText(loadStockDO.productName);
//        holder.tvDescription.setText(loadStockDO.productDescription);
//        holder.tvNumber.setText(""+loadStockDO.itemCount);



    }

    @Override
    public int getItemCount() {
        return 5;
    }

}
