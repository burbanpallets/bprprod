package com.tbs.generic.vansales.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.LoadStockMainDO
import com.tbs.generic.vansales.Model.NonScheduledProductMainDO
import com.tbs.generic.vansales.Model.ProductDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.PreferenceUtils

/**
 * Created by sandy on 2/15/2018.
 */

class AvailableLoadStockFragmment : Fragment() {
    lateinit var productDOS: List<ProductDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnCompleted: Button
    lateinit var loadStockMainDO: LoadStockMainDO
    lateinit var tvNoDataFound : TextView
    lateinit var scheduleDos : ArrayList<LoadStockDO>
    private var availableStockDos: java.util.ArrayList<LoadStockDO> = java.util.ArrayList()
    lateinit var nonScheduleDos : ArrayList<LoadStockDO>

    lateinit var nonScheduledProductMainDO: NonScheduledProductMainDO
    var driverID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        availableStockDos = arguments!!.getSerializable("availableData") as ArrayList<LoadStockDO>

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.unsheduled_fragment, container, false)
        recycleview = view.findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = view.findViewById<View>(R.id.tvNoDataFound) as TextView
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        var preferenceUtils = (activity as BaseActivity).preferenceUtils
        val name                  = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_NAME, "")
        val date                  = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE, "")
        driverID                          = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "QUA02")
        var routeId               = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")

        var isProductExisted = false

//        if(availableStockDos.size==0){

//            nonScheduleDos = arguments!!.getSerializable("NonScheduleLoadData") as ArrayList<LoadStockDO>
//            availableStockDos=nonScheduleDos
//            for (i in scheduleDos.indices) {
//                for (k in availableStockDos.indices) {
//                    if (scheduleDos.get(i).product.equals(availableStockDos!!.get(k).product,  true)) {
//                        availableStockDos!!.get(k).quantity= availableStockDos!!.get(k).quantity+scheduleDos.get(i).quantity
//                        isProductExisted = true
//                        break
//                    }
//                }
//                if (isProductExisted) {
//                    isProductExisted = false
//                    continue
//                } else {
//                    availableStockDos.add(scheduleDos.get(i))
//                }
//            }
//        }


        if (availableStockDos!=null && availableStockDos.size>0) {
            val loadStockAdapter = LoadStockAdapter(context, availableStockDos, "")
            recycleview.adapter = loadStockAdapter
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else {
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return view
    }
}

    private operator fun Double.plus(load: String?): Any {
        return load!!
    }
