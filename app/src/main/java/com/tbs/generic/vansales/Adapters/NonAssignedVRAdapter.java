package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.app.Dialog;
import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.DriverEmptyDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class NonAssignedVRAdapter extends RecyclerView.Adapter<NonAssignedVRAdapter.MyViewHolder> {

    private ArrayList<DriverEmptyDO> pickUpDos;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId, tvTripNumber, tvVehicleCode, tvPlate, tvDriver, tvDDT;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvSiteName = view.findViewById(R.id.tvSiteName);
            tvSiteId = view.findViewById(R.id.tvVrId);
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            tvTripNumber = view.findViewById(R.id.tvTripNumber);
            tvDriver = view.findViewById(R.id.tvDriver);
            tvVehicleCode = view.findViewById(R.id.tvVehicleCode);
            tvPlate = view.findViewById(R.id.tvPlate);
            tvDDT = view.findViewById(R.id.tvDDT);
        }
    }

    private Dialog scheduleddialog;

    public NonAssignedVRAdapter(Context context, ArrayList<DriverEmptyDO> siteDOS, Dialog scheduleddialog) {
        this.context = context;
        this.pickUpDos = siteDOS;
        this.scheduleddialog = scheduleddialog;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.empty_route_id_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // ((LinearLayout)holder.llDetails).setBackgroundColor(Color.CYAN);
        final DriverEmptyDO siteDO = pickUpDos.get(position);
        holder.tvSiteName.setText(context.getString(R.string.site) + " : " + siteDO.site);
        holder.tvSiteId.setText(siteDO.emptyVehicleCode);

        //  holder.tvSiteId.setText("" + siteDO.siteId);
        holder.tvVehicleCode.setText(context.getString(R.string.vehicle_code) + siteDO.vehicleCode);
        holder.tvPlate.setText(context.getString(R.string.plate_label) + siteDO.plate);
        holder.tvTripNumber.setText(context.getString(R.string.trip_number) + " : " + siteDO.tripNumber);
        holder.tvDriver.setText(context.getString(R.string.driver) + " : " + siteDO.driver);
//        if(row_index==position){
//            holder.llDetails.setBackgroundColor(Color.parseColor("#567845"));
//        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
//                preferenceUtils.saveString(PreferenceUtils.EM_CARRIER_ID, String.valueOf(pickUpDos.get(pos).emptyVehicleCode));
//
//                preferenceUtils.saveString(PreferenceUtils.EM_CARRIER_ID, String.valueOf(pickUpDos.get(pos).emptyVehicleCode));
//
//                ((CheckinActivity)context).tvSelection.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.EM_CARRIER_ID, ""));
//                ((CheckinActivity)context).dialog.dismiss();

                // ((CheckinActivity)context).dialog.dismiss();
//                Intent intent = new Intent(context,CheckinActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                context.startActivity(intent);

                if (scheduleddialog != null) {
                    scheduleddialog.dismiss();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return pickUpDos.size();
    }

}
