//package com.tbs.generic.vansales.Adapters;
//
///**
// * Created by sandy on 2/7/2018.
// */
//
//import android.content.Context;
//import android.content.Intent;
//import androidx.recyclerview.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.tbs.generic.vansales.Activitys.BaseActivity;
//import com.tbs.generic.vansales.Activitys.PriceDetailsActivity;
//import com.tbs.generic.vansales.Model.PriceItemDO;
//import com.tbs.generic.vansales.R;
//
//import java.util.ArrayList;
//
//public class PriceItemAdapter extends RecyclerView.Adapter<PriceItemAdapter.MyViewHolder>  {
//
//    private ArrayList<PriceItemDO> priceDOS;
//    private Context context;
//
//
//
//
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public TextView tvPriceId, tvPriceValue,tvPriceName;
//        private LinearLayout llDetails;
//
//        public MyViewHolder(View view) {
//            super(view);
//            llDetails = view.findViewById(R.id.llDetails);
//            tvPriceId = view.findViewById(R.id.tvPriceId);
//            tvPriceName = view.findViewById(R.id.tvPriceName);
//            tvPriceValue = view.findViewById(R.id.tvPriceValue);
//
//
//        }
//    }
//
//
//    public PriceItemAdapter(Context context, ArrayList<PriceItemDO> priceDOS) {
//        this.context = context;
//        this.priceDOS = priceDOS;
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.price_item_data, parent, false);
//
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(final MyViewHolder holder, int position) {
//        if(priceDOS.size()>0){
//
//
//        final PriceItemDO siteDO = priceDOS.get(position);
//        holder. tvPriceId.setText(context.getString(R.string.record)+" : " + priceDOS.get(position).record);
//        holder. tvPriceName.setText(context.getString(R.string.start_Date)+" : " + priceDOS.get(position).startDate);
//        holder.tvPriceValue.setText(context.getString(R.string.end_date)+" : " + priceDOS.get(position).endDate);
//        holder.llDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(context, PriceDetailsActivity.class);
//                intent.putExtra("Record", siteDO.record);
//                context.startActivity(intent);
//
//
//            }
//        });
//
//        }else {
//            ((BaseActivity)context).showAlert(context.getString(R.string.no_records_found));
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return priceDOS.size();
//    }
//
//}
