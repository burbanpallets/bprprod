package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.CustomerDetailsActivity;
import com.tbs.generic.vansales.Activitys.EquipmentselectionScreen;
import com.tbs.generic.vansales.Activitys.SerializationActivity;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ProductSelectionAdapter extends RecyclerView.Adapter<ProductSelectionAdapter.MyViewHolder> {

    private ArrayList<TrailerSelectionDO> trailerSelectionDOS;
    private Context context;


    public void refreshAdapter(@NotNull ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        this.trailerSelectionDOS = trailerSelectionDOS;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPO, tvLoan, tvCustomer, tvEquiment, tvQty;
        private CheckBox cbSelected;
        LinearLayout llPO;

        public MyViewHolder(View view) {
            super(view);
            tvPO = itemView.findViewById(R.id.tvPONumber);
            tvLoan = itemView.findViewById(R.id.tvLoanDelivery);
            tvCustomer = itemView.findViewById(R.id.tvCustomer);
            tvEquiment = itemView.findViewById(R.id.tvEquioment);
            tvQty = itemView.findViewById(R.id.tvQty);
            cbSelected = itemView.findViewById(R.id.cbSelected);
            llPO = itemView.findViewById(R.id.llPO);


        }
    }


    public ProductSelectionAdapter(Context context, ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        this.context = context;
        this.trailerSelectionDOS = trailerSelectionDOS;

    }

    private ArrayList<TrailerSelectionDO> selectedDOs = new ArrayList<>();

    public ArrayList<TrailerSelectionDO> getSelectedTrailerDOs() {
        return selectedDOs;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_selection_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final TrailerSelectionDO trailerSelectionDO = trailerSelectionDOS.get(position);
//        final VRSelectionDO vrSelectionDO = new VRSelectionDO();
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lg));
        } else {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lsg));
        }
if(!trailerSelectionDO.poNumber.isEmpty()){
    holder.tvPO.setText(trailerSelectionDO.poNumber);
    holder.llPO.setVisibility(View.VISIBLE);

}else {
    holder.llPO.setVisibility(View.GONE);

}

        holder.tvLoan.setText(trailerSelectionDO.loandelivery);
        holder.tvCustomer.setText(trailerSelectionDO.customer+" - "+trailerSelectionDO.customerDescription);
        holder.tvEquiment.setText(trailerSelectionDO.trailer+" - "+trailerSelectionDO.trailerDescription);

        holder.tvQty.setText(""+trailerSelectionDO.quantity);


//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent= new Intent(context, SerializationActivity.class);
//                intent.putExtra("PRODUCTDO", trailerSelectionDO);
//                ((BaseActivity) context).startActivityForResult(intent,10);
//
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return trailerSelectionDOS.size();
//        return 10;

    }

}
